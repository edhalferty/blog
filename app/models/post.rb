class Post < ActiveRecord::Base
  has_many :comments

  validates_presence_of :body
  validates_presence_of :title

  #validates :title, :body, presence: true
end
